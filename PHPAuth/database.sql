-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server versie:                5.6.26 - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL Versie:              9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
-- Dumpen data van tabel twz.attempts: ~0 rows (ongeveer)
/*!40000 ALTER TABLE `attempts` DISABLE KEYS */;
REPLACE INTO `attempts` (`id`, `ip`, `count`, `expiredate`) VALUES
	(2, '::1', 1, '2015-09-29 14:51:13');
/*!40000 ALTER TABLE `attempts` ENABLE KEYS */;

-- Dumpen data van tabel twz.config: ~26 rows (ongeveer)
/*!40000 ALTER TABLE `config` DISABLE KEYS */;
REPLACE INTO `config` (`id`, `setting`, `value`) VALUES
	(1, 'site_name', 'PHPAuth'),
	(2, 'site_url', 'http://localhost/twz/PHPAuth'),
	(3, 'site_email', 'no-reply@phpauth.cuonic.com'),
	(4, 'cookie_name', 'authID'),
	(5, 'cookie_path', '/'),
	(6, 'cookie_domain', NULL),
	(7, 'cookie_secure', '0'),
	(8, 'cookie_http', '0'),
	(9, 'site_key', 'fghuior.)/!/jdUkd8s2!7HVHG7777ghg'),
	(10, 'cookie_remember', '+1 month'),
	(11, 'cookie_forget', '+30 minutes'),
	(12, 'bcrypt_cost', '10'),
	(13, 'table_attempts', 'attempts'),
	(14, 'table_requests', 'requests'),
	(15, 'table_sessions', 'sessions'),
	(16, 'table_users', 'users'),
	(17, 'site_timezone', 'Europe/Paris'),
	(18, 'site_activation_page', 'activate'),
	(19, 'site_password_reset_page', 'reset'),
	(20, 'smtp', '0'),
	(21, 'smtp_host', 'smtp.example.com'),
	(22, 'smtp_auth', '1'),
	(23, 'smtp_username', 'email@example.com'),
	(24, 'smtp_password', 'password'),
	(25, 'smtp_port', '25'),
	(26, 'smtp_security', NULL);
/*!40000 ALTER TABLE `config` ENABLE KEYS */;

-- Dumpen data van tabel twz.opleiding: ~0 rows (ongeveer)
/*!40000 ALTER TABLE `opleiding` DISABLE KEYS */;
REPLACE INTO `opleiding` (`ID`, `Naam`) VALUES
	(0, '0');
/*!40000 ALTER TABLE `opleiding` ENABLE KEYS */;

-- Dumpen data van tabel twz.requests: ~2 rows (ongeveer)
/*!40000 ALTER TABLE `requests` DISABLE KEYS */;
REPLACE INTO `requests` (`id`, `uid`, `rkey`, `expire`, `type`) VALUES
	(1, 2, 'h3erPv19w4w437176M33', '2015-09-30 14:19:38', 'activation'),
	(2, 3, 'a3F753xGX49lmdcxumzN', '2015-09-30 14:29:57', 'activation');
/*!40000 ALTER TABLE `requests` ENABLE KEYS */;

-- Dumpen data van tabel twz.sessions: ~0 rows (ongeveer)
/*!40000 ALTER TABLE `sessions` DISABLE KEYS */;
REPLACE INTO `sessions` (`id`, `uid`, `hash`, `expiredate`, `ip`, `agent`, `cookie_crc`) VALUES
	(4, 1, 'fc5b1635792575daf72ca77eaf021801ba56a4c2', '2015-10-29 09:21:22', '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:40.0) Gecko/20100101 Firefox/40.0', '4e2943088dc93b1497ca1b541190d28b0ccf1ad7');
/*!40000 ALTER TABLE `sessions` ENABLE KEYS */;

-- Dumpen data van tabel twz.surveillant: ~2 rows (ongeveer)
/*!40000 ALTER TABLE `surveillant` DISABLE KEYS */;
REPLACE INTO `surveillant` (`WerknemerID`, `Actief`, `Voornaam`, `Achternaam`, `GebruikerID`, `Tussenvoegsel`) VALUES
	(420, 0, '0', '0', 0, '0'),
	(1337, 0, 'Mathijs', 'Knooier', 3, 'de');
/*!40000 ALTER TABLE `surveillant` ENABLE KEYS */;

-- Dumpen data van tabel twz.tentamen: ~0 rows (ongeveer)
/*!40000 ALTER TABLE `tentamen` DISABLE KEYS */;
/*!40000 ALTER TABLE `tentamen` ENABLE KEYS */;

-- Dumpen data van tabel twz.users: ~3 rows (ongeveer)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
REPLACE INTO `users` (`id`, `email`, `password`, `salt`, `isactive`, `dt`, `rank`) VALUES
	(1, 'pagt0007@hz.nl', '$2y$10$jx1Ll.2GIdN2i7m5PUuMm.ZelKBgXwZTROuR8U0bT2Jn9lGEfB8xy', NULL, 1, '2015-09-28 11:31:51', 'admin'),
	(2, 'vos0043@hz.nl', '$2y$10$qgvEUMDQpZNy53z0q4yJ3e1T2PxPeM8ui.LpBVX57A1.3OdY2dZui', NULL, 0, '2015-09-29 12:19:38', '0'),
	(3, 'nooi0047@hz.nl', '$2y$10$rY0TkX8HCSmzpdbyCXBY9uf22HFMCHZzMdZ9QmSdrBay.925DPXBa', NULL, 0, '2015-09-29 12:29:56', '0');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
