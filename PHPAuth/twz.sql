-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server versie:                5.6.26 - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL Versie:              9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Databasestructuur van twz wordt geschreven
DROP DATABASE IF EXISTS `twz`;
CREATE DATABASE IF NOT EXISTS `twz` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */;
USE `twz`;


-- Structuur van  tabel twz.attempts wordt geschreven
DROP TABLE IF EXISTS `attempts`;
CREATE TABLE IF NOT EXISTS `attempts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip` varchar(39) NOT NULL,
  `count` int(11) NOT NULL,
  `expiredate` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumpen data van tabel twz.attempts: ~0 rows (ongeveer)
DELETE FROM `attempts`;
/*!40000 ALTER TABLE `attempts` DISABLE KEYS */;
/*!40000 ALTER TABLE `attempts` ENABLE KEYS */;


-- Structuur van  tabel twz.beschikbaarheid wordt geschreven
DROP TABLE IF EXISTS `beschikbaarheid`;
CREATE TABLE IF NOT EXISTS `beschikbaarheid` (
  `SurveillantID` int(11) NOT NULL,
  `Datum` date NOT NULL DEFAULT '0000-00-00',
  `Ochtend` tinyint(1) DEFAULT NULL,
  `Middag` tinyint(1) DEFAULT NULL,
  `Avond` tinyint(1) NOT NULL,
  PRIMARY KEY (`SurveillantID`,`Datum`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumpen data van tabel twz.beschikbaarheid: ~26 rows (ongeveer)
DELETE FROM `beschikbaarheid`;
/*!40000 ALTER TABLE `beschikbaarheid` DISABLE KEYS */;
INSERT INTO `beschikbaarheid` (`SurveillantID`, `Datum`, `Ochtend`, `Middag`, `Avond`) VALUES
	(1, '2015-10-12', 1, 1, 1),
	(1, '2015-10-13', 1, 1, 0),
	(1, '2015-10-14', 1, NULL, 0),
	(1, '2015-10-15', 1, 1, 1),
	(1, '2015-10-16', 1, 1, 1),
	(1, '2015-10-19', 1, 1, 1),
	(2, '2015-10-12', 1, 1, 1),
	(2, '2015-10-13', 1, 1, 1),
	(2, '2015-10-14', 1, 1, 1),
	(2, '2015-10-15', 1, 1, 1),
	(2, '2015-10-16', NULL, NULL, 0),
	(2, '2015-10-19', 1, 1, 1),
	(2, '2015-10-20', 1, 1, 1),
	(2, '2015-10-21', 1, 1, 1),
	(3, '2015-10-14', 1, 1, 0),
	(3, '2015-10-19', 1, 1, 1),
	(5, '2015-10-12', 1, 1, 1),
	(5, '2015-10-13', 1, 1, 1),
	(5, '2015-10-14', 1, 1, 1),
	(5, '2015-10-15', 1, 1, 1),
	(5, '2015-10-16', 1, 1, 1),
	(5, '2015-10-19', 1, 1, 1),
	(5, '2015-10-20', 1, 1, 1),
	(5, '2015-10-21', 1, 1, 1),
	(5, '2015-10-22', 1, 1, 1),
	(5, '2015-10-23', 1, 1, 1);
/*!40000 ALTER TABLE `beschikbaarheid` ENABLE KEYS */;


-- Structuur van  tabel twz.config wordt geschreven
DROP TABLE IF EXISTS `config`;
CREATE TABLE IF NOT EXISTS `config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `setting` varchar(100) NOT NULL,
  `value` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;

-- Dumpen data van tabel twz.config: ~26 rows (ongeveer)
DELETE FROM `config`;
/*!40000 ALTER TABLE `config` DISABLE KEYS */;
INSERT INTO `config` (`id`, `setting`, `value`) VALUES
	(1, 'site_name', 'PHPAuth'),
	(2, 'site_url', 'http://localhost/twz/PHPAuth'),
	(3, 'site_email', 'no-reply@phpauth.cuonic.com'),
	(4, 'cookie_name', 'authID'),
	(5, 'cookie_path', '/'),
	(6, 'cookie_domain', NULL),
	(7, 'cookie_secure', '0'),
	(8, 'cookie_http', '0'),
	(9, 'site_key', 'fghuior.)/!/jdUkd8s2!7HVHG7777ghg'),
	(10, 'cookie_remember', '+1 month'),
	(11, 'cookie_forget', '+30 minutes'),
	(12, 'bcrypt_cost', '10'),
	(13, 'table_attempts', 'attempts'),
	(14, 'table_requests', 'requests'),
	(15, 'table_sessions', 'sessions'),
	(16, 'table_users', 'users'),
	(17, 'site_timezone', 'Europe/Paris'),
	(18, 'site_activation_page', 'activate'),
	(19, 'site_password_reset_page', 'reset'),
	(20, 'smtp', '0'),
	(21, 'smtp_host', 'smtp.example.com'),
	(22, 'smtp_auth', '1'),
	(23, 'smtp_username', 'email@example.com'),
	(24, 'smtp_password', 'password'),
	(25, 'smtp_port', '25'),
	(26, 'smtp_security', NULL);
/*!40000 ALTER TABLE `config` ENABLE KEYS */;


-- Structuur van  tabel twz.opleiding wordt geschreven
DROP TABLE IF EXISTS `opleiding`;
CREATE TABLE IF NOT EXISTS `opleiding` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Naam` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumpen data van tabel twz.opleiding: ~6 rows (ongeveer)
DELETE FROM `opleiding`;
/*!40000 ALTER TABLE `opleiding` DISABLE KEYS */;
INSERT INTO `opleiding` (`ID`, `Naam`) VALUES
	(3, 'Marinerings Officier HBO'),
	(4, 'Vliegvaartuigische Bouwkunde'),
	(5, 'HBO Mosselkritiek'),
	(6, 'Consulitatie'),
	(7, 'Aquatische Destructie'),
	(8, 'Cosmetische Weergeschiedenis');
/*!40000 ALTER TABLE `opleiding` ENABLE KEYS */;


-- Structuur van  tabel twz.requests wordt geschreven
DROP TABLE IF EXISTS `requests`;
CREATE TABLE IF NOT EXISTS `requests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `rkey` varchar(20) NOT NULL,
  `expire` datetime NOT NULL,
  `type` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Dumpen data van tabel twz.requests: ~2 rows (ongeveer)
DELETE FROM `requests`;
/*!40000 ALTER TABLE `requests` DISABLE KEYS */;
INSERT INTO `requests` (`id`, `uid`, `rkey`, `expire`, `type`) VALUES
	(1, 2, 'h3erPv19w4w437176M33', '2015-09-30 14:19:38', 'activation'),
	(2, 3, 'a3F753xGX49lmdcxumzN', '2015-09-30 14:29:57', 'activation');
/*!40000 ALTER TABLE `requests` ENABLE KEYS */;


-- Structuur van  tabel twz.sessions wordt geschreven
DROP TABLE IF EXISTS `sessions`;
CREATE TABLE IF NOT EXISTS `sessions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `hash` varchar(40) NOT NULL,
  `expiredate` datetime NOT NULL,
  `ip` varchar(39) NOT NULL,
  `agent` varchar(200) NOT NULL,
  `cookie_crc` varchar(40) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

-- Dumpen data van tabel twz.sessions: ~1 rows (ongeveer)
DELETE FROM `sessions`;
/*!40000 ALTER TABLE `sessions` DISABLE KEYS */;
INSERT INTO `sessions` (`id`, `uid`, `hash`, `expiredate`, `ip`, `agent`, `cookie_crc`) VALUES
	(20, 1, '28e1ea43d52921e96cbbc129d73d424e8edd7f6b', '2015-11-07 09:18:27', '::1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.101 Safari/537.36', '8b2afa89ca101b5f50922363f5592d85ca1d6dd4');
/*!40000 ALTER TABLE `sessions` ENABLE KEYS */;


-- Structuur van  tabel twz.surveillant wordt geschreven
DROP TABLE IF EXISTS `surveillant`;
CREATE TABLE IF NOT EXISTS `surveillant` (
  `WerknemerID` int(11) NOT NULL,
  `Actief` tinyint(1) NOT NULL,
  `Voornaam` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `GebruikerID` int(11) NOT NULL,
  `Achternaam` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Tussenvoegsel` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumpen data van tabel twz.surveillant: ~6 rows (ongeveer)
DELETE FROM `surveillant`;
/*!40000 ALTER TABLE `surveillant` DISABLE KEYS */;
INSERT INTO `surveillant` (`WerknemerID`, `Actief`, `Voornaam`, `GebruikerID`, `Achternaam`, `ID`, `Tussenvoegsel`) VALUES
	(1337, 1, 'Mathijs', 0, 'Knooier', 1, 'de'),
	(420, 1, 'Jan', 2, 'Vries', 2, 'de'),
	(476674643, 1, 'Henk', 3, 'Jager', 3, 'de'),
	(94329, 1, 'Peter', 0, 'Straat', 4, 'van de'),
	(6732, 1, 'Betje', 0, 'Ketelmaker', 5, 'de'),
	(72735654, 1, 'Wiz', 0, 'Khalifa', 6, '');
/*!40000 ALTER TABLE `surveillant` ENABLE KEYS */;


-- Structuur van  tabel twz.tentamen wordt geschreven
DROP TABLE IF EXISTS `tentamen`;
CREATE TABLE IF NOT EXISTS `tentamen` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Naam` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `Opmerking` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `Aantal` int(11) NOT NULL,
  `Dag` date NOT NULL,
  `BeginTijd` time(6) NOT NULL,
  `EindTijd` time(6) NOT NULL,
  `OpleidingID` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumpen data van tabel twz.tentamen: ~5 rows (ongeveer)
DELETE FROM `tentamen`;
/*!40000 ALTER TABLE `tentamen` DISABLE KEYS */;
INSERT INTO `tentamen` (`ID`, `Naam`, `Opmerking`, `Aantal`, `Dag`, `BeginTijd`, `EindTijd`, `OpleidingID`) VALUES
	(1, 'Vistank techniek', 'Ik hoop dat jullie voorbereid zijn!', 2, '2015-10-15', '08:45:00.000000', '09:45:00.000000', 3),
	(2, 'Propellorbouw', 'veel leren', 2, '2015-10-19', '09:00:00.000000', '09:30:00.000000', 4),
	(3, 'Consulitatie 101', 'Goed leren jongens en meisjes, dit is een moeilijkerd!', 3, '2015-10-13', '08:45:00.000000', '09:40:00.000000', 6),
	(4, 'Hoge gezichts bedrukking', 'goede antwoorden leveren alleen punten op', 3, '2015-10-15', '09:00:00.000000', '12:30:00.000000', 8),
	(5, 'Dubbeltentamen!', 'shit is crazy', 2, '2015-10-15', '09:00:00.000000', '12:00:00.000000', 8);
/*!40000 ALTER TABLE `tentamen` ENABLE KEYS */;


-- Structuur van  tabel twz.tentamensurveillant wordt geschreven
DROP TABLE IF EXISTS `tentamensurveillant`;
CREATE TABLE IF NOT EXISTS `tentamensurveillant` (
  `TentamenID` int(11) NOT NULL DEFAULT '0',
  `SurveillantID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`SurveillantID`,`TentamenID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumpen data van tabel twz.tentamensurveillant: ~9 rows (ongeveer)
DELETE FROM `tentamensurveillant`;
/*!40000 ALTER TABLE `tentamensurveillant` DISABLE KEYS */;
INSERT INTO `tentamensurveillant` (`TentamenID`, `SurveillantID`) VALUES
	(2, 1),
	(3, 1),
	(5, 1),
	(1, 2),
	(2, 2),
	(3, 2),
	(1, 3),
	(3, 5),
	(5, 5);
/*!40000 ALTER TABLE `tentamensurveillant` ENABLE KEYS */;


-- Structuur van  tabel twz.users wordt geschreven
DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(100) DEFAULT NULL,
  `password` varchar(60) DEFAULT NULL,
  `salt` varchar(120) DEFAULT NULL,
  `isactive` tinyint(1) NOT NULL DEFAULT '0',
  `dt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `rank` varchar(50) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Dumpen data van tabel twz.users: ~3 rows (ongeveer)
DELETE FROM `users`;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `email`, `password`, `salt`, `isactive`, `dt`, `rank`) VALUES
	(1, 'pagt0007@hz.nl', '$2y$10$jx1Ll.2GIdN2i7m5PUuMm.ZelKBgXwZTROuR8U0bT2Jn9lGEfB8xy', NULL, 1, '2015-09-28 09:31:51', 'admin'),
	(2, 'vos0043@hz.nl', '$2y$10$jx1Ll.2GIdN2i7m5PUuMm.ZelKBgXwZTROuR8U0bT2Jn9lGEfB8xy', NULL, 1, '2015-09-29 10:19:38', 'user'),
	(3, 'nooi0047@hz.nl', '$2y$10$rY0TkX8HCSmzpdbyCXBY9uf22HFMCHZzMdZ9QmSdrBay.925DPXBa', NULL, 0, '2015-09-29 10:29:56', '0');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
