<?php
session_start();
require_once '../lib/connectdb.php';
require_once '../lib/functions.php';
require_once '../lib/requireAuth.php';
require_once '../lib/requireSession.php';
require_once '../lib/requireAdmin.php';

//Checks if you already put in a year, if so makes the session variable the Post value of jaar
if (isset($_POST['Jaar'])) {
	$_SESSION['Jaar'] = $_POST['Jaar'];
}
if (isset($_POST['Week'])) {
	$_SESSION['Week'] = $_POST['Week'];
}


//If there is no Session value for Jaar, makes it the current year. Failsafe
if (!isset($_SESSION['Jaar'])) {
	$now = new DateTime();
	$currentyear = $now -> format('Y');
	$_SESSION['Jaar'] = $currentyear;
}
$tentamenjaren = $dataManager->rawQuery('SELECT DISTINCT EXTRACT(Year from `dag`) AS Jaar FROM Tentamen ORDER BY Jaar ASC');
$tentamenweken = $dataManager->rawQuery('SELECT DISTINCT WEEK(`dag`,3) AS Week FROM Tentamen WHERE EXTRACT(Year from `dag`)=   ' . mysql_real_escape_string( $_SESSION['Jaar']) .  ' ORDER BY Week ASC');

?>
<!DOCTYPE html>
<html lang="en">

<head>

    <title>TWZ - Rooster</title>

    <?php include_once "../includes/head.php"; ?>

</head>
<body>

<div id="wrapper">

    <!-- Navigation -->
    <?php include_once "../includes/nav.php"; ?>

    <div id="page-wrapper">
    	
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Rooster
                    <small>Overzicht</small>
                </h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->

        <div class="row">
        	
        	<form action="timetable.php" method="post">
                    
                    <div class="form-group">
                        <label >Selecteer een jaar</label>
                        <select class="form-control" name="Jaar" onchange="form.submit()">
                            <option value="NULL">Selecteer een jaar</option>
                            
                            <?php

							foreach ($tentamenjaren as $tentamenjaar) {

								$y = $tentamenjaar['Jaar'];
								//Keeps the currently selected year through refreshes, as its a session variable now
								if (isset($_SESSION['Jaar']) && $_SESSION['Jaar'] == $y) {
									echo '<option value="' . $y . '" selected>Jaar ' . $y . '</option>';
								} else {
									echo '<option value="' . $y . '">Jaar ' . $y . '</option>';
								}

							}
                            ?>
                        </select>
                    </div>
                </form>

             
                <form action="timetable.php" method="post">
                    
                    <div class="form-group">
                        <label >Selecteer een week</label>
                        <select class="form-control" name="Week" onchange="form.submit()">
                            <option value="NULL">Selecteer een week</option>
                            <?php
                           
							foreach ($tentamenweken as $tentamenweek) {
								$w = $tentamenweek['Week'];
								//Will keep the current selection if you change week, not if you change years. working as intended
								if (isset($_SESSION['Jaar'])) {
									if (isset($_SESSION['Week']) && $_SESSION['Week'] == $w) {
										echo '<option value="' . $w . '" selected>Week ' . $w . '</option>';
									} else {
										echo '<option value="' . $w . '">Week ' . $w . '</option>';
									}
								}

							}
                            ?>
                        </select>
                    </div>
                </form>
               

        </div>
        <div class="row">

            <?php

                if(isset($_SESSION['Week'])) {
                    $week = $_SESSION['Week'];
                    if(validateInput($week, 1, 2)) {

            ?>

            <div class="table-responsive">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>Data &#9660;</th>
                            <th>Maandag</th>
                            <th>Dinsdag</th>
                            <th>Woensdag</th>
                            <th>Donderdag</th>
                            <th>Vrijdag</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php

                        $dataManager->join("TentamenSurveillant ts", "t.ID=ts.TentamenID", "LEFT");

                        $dataManager->where('Week(dag,3)', $week);
						$dataManager -> where('Year(`dag`)', $_SESSION['Jaar']);

                        $dataManager->orderBy('Dag', 'ASC');
                        $dataManager->orderBy('BeginTijd', 'ASC');
                        $dataManager->orderBy('EindTijd', 'ASC');
                        $dataManager->groupBy('ID');

                        $tentamens= $dataManager->get('Tentamen t', null,
                            't.*, COUNT(ts.SurveillantID) AS AantalSurveillantenToegewezen'
                        );

                        foreach($tentamens as $tentamen) {
                            $datum = new DateTime($tentamen['Dag']);
                            $dag = $datum->format('D');

                            $beginTijd = new DateTime($tentamen['BeginTijd']);
                            $beginTijd = $beginTijd->format('H:i');

                            $eindTijd = new DateTime($tentamen['EindTijd']);
                            $eindTijd = $eindTijd->format('H:i');

                            $buttonClassState = '';

                            if($tentamen['Aantal'] == $tentamen['AantalSurveillantenToegewezen']) {
                                $buttonClassState = 'btn-success';
                            }
                            else if($tentamen['Aantal'] > $tentamen['AantalSurveillantenToegewezen']) {
                                $buttonClassState = 'btn-warning';
                            }
                            else {
                                $buttonClassState = 'btn-danger';
                            }

                            echo '<tr>';

                            switch($dag) {
                                case 'Mon':
                                    echo '<td>' . $beginTijd . '-' . $eindTijd . '</td>';
                                    echo '<td colspan="5"><a href="timetable_details.php?id=' . $tentamen['ID'] . '" class="btn btn-outline ' . $buttonClassState . '">' . $tentamen['Naam'] . '</a></td>';
                                    break;
                                case 'Tue':
                                    echo '<td colspan="2">' . $beginTijd . '-' . $eindTijd . '</td>';
                                    echo '<td colspan="4"><a href="timetable_details.php?id=' . $tentamen['ID'] . '" class="btn btn-outline ' . $buttonClassState . '">' . $tentamen['Naam'] . '</a></td>';
                                    break;
                                case 'Wed':
                                    echo '<td colspan="3">' . $beginTijd . '-' . $eindTijd . '</td>';
                                    echo '<td colspan="3"><a href="timetable_details.php?id=' . $tentamen['ID'] . '" class="btn btn-outline ' . $buttonClassState . '">' . $tentamen['Naam'] . '</a></td>';
                                    break;
                                case 'Thu':
                                    echo '<td colspan="4">' . $beginTijd . '-' . $eindTijd . '</td>';
                                    echo '<td colspan="2"><a href="timetable_details.php?id=' . $tentamen['ID'] . '" class="btn btn-outline ' . $buttonClassState . '">' . $tentamen['Naam'] . '</a></td>';
                                    break;
                                case 'Fri':
                                    echo '<td colspan="5">' . $beginTijd . '-' . $eindTijd . '</td>';
                                    echo '<td><a href="timetable_details.php?id=' . $tentamen['ID'] . '" class="btn btn-outline ' . $buttonClassState . '">' . $tentamen['Naam'] . '</a></td>';
                                    break;
                            }

                            echo '</tr>';

                        }

                    ?>
                    </tbody>
                </table>
            </div>
            <!-- /.table-responsive -->

            <?php
                    }
                }
            ?>

        </div>
        <!-- /.row -->
    </div>
    <!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

    <?php include_once "../includes/footer.php"; ?>

</body>

</html>
